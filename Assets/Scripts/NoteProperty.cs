﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteProperty
{
	public float beatBegin; //beat : ノーツ 始点 が判定ラインと重なる
	public float beatEnd; //beat : ノーツ 終点 が判定ラインと重なる
	public int lane; //レーン
	public NoteType noteType; //ノーツ種別

	// クラスコンストラクタ
	public NoteProperty(float beatBegin, float beatEnd, int lane, NoteType noteType)
	{
		this.beatBegin = beatBegin;
		this.beatEnd = beatEnd;
		this.lane = lane;
		this.noteType = noteType;
	}

}

public enum NoteType
{
	Single, //シングルノーツ (0 として扱われる)
	Long //ロングノーツ (1 として扱われる)
}
