﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleNoteController : NoteControllerBase
{
	// Update is called once per frame
	void Update()
	{
		// ノーツの座標
		Vector2 position = new Vector2();

		// ノーツが流れてくるレーン設定(中央が第2レーンのため調整)
		position.x = noteProperty.lane - 2;

		// 曲スタートからノーツが判定に来るまでの時間 - 今までの経過時間
		position.y = (noteProperty.beatBegin - PlayerController.CurrentBeat) *
			PlayerController.ScrollSpeed;

		// 親のTransformオブジェクトから見た相対位置
		transform.localPosition = position;
	}
}
