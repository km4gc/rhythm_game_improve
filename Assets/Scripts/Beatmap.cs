﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;

public class Beatmap
{
	public List<NoteProperty> noteProperties = new List<NoteProperty>();
	public List<TempoChange> tempoChanges = new List<TempoChange>();

	// 指定テンポで、beat --> sec 変換
	public static float ToSecWithFixedTempo(float beat, float tempo)
	{
		return beat / (tempo / 60f);
	}

	// 指定テンポで、sec --> beat 変換
	public static float ToBeatWithFixedTempo(float sec, float tempo)
	{
		return sec * (tempo / 60f);
	}

	// テンポ変化情報をもとに、beat --> sec 変換
	public static float ToSec(float beat, List<TempoChange> tempoChanges)
	{
		float accumulatedSec = 0f; // 累計秒数
		int i = 0; // テンポ変化番号
		var n = tempoChanges.Count(x => x.beat <= beat); // テンポ変化の回数 : 変換するbeatの直前まで

		// 変換するbeatの直前にあるテンポ変化までのsecを求める
		while(i < n - 1)
		{
			accumulatedSec += ToSecWithFixedTempo(
				tempoChanges[i + 1].beat - tempoChanges[i].beat,
				tempoChanges[i].tempo
			);
			i++;
		}
		// 残りのbeat分を加算
		accumulatedSec += ToSecWithFixedTempo(
			beat - tempoChanges[i].beat, tempoChanges[i].tempo
		);
		return accumulatedSec;
	}

	// テンポ変化情報をもとに、sec --> beat 変換
	public static float ToBeat(float sec, List<TempoChange> tempoChanges)
	{
		float accumulatedSec = 0f; // 累計秒数
		int i = 0; // テンポ変化番号
		var n = tempoChanges.Count; // テンポ変化の回数 : 全テンポ変化

		// 最後から一つ前のテンポ変化までループ
		while (i < n - 1)
		{
			var tmpSec = accumulatedSec; // i回目のテンポ変化地点での秒数

			// 次(i+1回目)のテンポ変化タイミング(sec.)を計算
			accumulatedSec += ToSecWithFixedTempo(
				tempoChanges[i+1].beat - tempoChanges[i].beat,
				tempoChanges[i].tempo
			);
			if(accumulatedSec >= sec)
			{
				// 次のテンポ変化 > 変換するsec
				// 「超える直前のテンポ変化があるbeat + 残りのbeat」を返す
				return tempoChanges[i].beat +
					ToBeatWithFixedTempo(
						sec - tmpSec, tempoChanges[i].tempo
					);
			}
			i++;
		}

		// 変換するsec > 最後のテンポ変化
		// 「最後のテンポ変化があるbeat + 残りのbeat」を返す
		return tempoChanges[n-1].beat +
			ToBeatWithFixedTempo(
			sec - accumulatedSec, tempoChanges[n-1].tempo
			);
	}
}
